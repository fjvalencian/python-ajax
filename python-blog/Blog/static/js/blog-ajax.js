$(function() {
  function create_post() {
    console.log("create post is working!") // sanity check
    $.ajax({
        url : "add_post/",
        type : "POST",
        data : { post_text: $('#text_area').val()},
        success : function(json) {
            $('#text_area').val(''); // remove the value from the input
            console.log(json); // log the returned json to the console
            
            $("#posts").append("<tr><td class='card'>"+json.id+"</td><td>"+json.author+"</td><td>"+json.text+"</td><td>"+json.comentarios+"</td><td class='remove-post-button'><button>Borrar</button></td><td class='duplicar'><button onClick='window.location.reload()'>Duplicar</button></td><td class='ver_post'><button>Ver post</button></td>");            
            console.log("success"); // another sanity check
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
  }

    $(document).ready( function() {
    $('#post_form').on('submit', function(event){
        event.preventDefault();
        console.log("form submitted!")  // sanity check
        create_post();
    });
  });


  function login_failed() {
    console.log("login is working!") // sanity check
    $.ajax({
        url : "add_post/",
        type : "POST",
        data : { username: $('#username-field').val(),
                 password: $('#password-field').val()},
        success : function(json) {
            if (json.redirect) {
            // data.redirect contains the string URL to redirect to
              window.location.href = json.redirect;
            }
            else {
              $('#password-field').val('');
              console.log(json); // log the returned json to the console
              $("#failed-notice").html("GG WP LOGIN");
              console.log("success"); // another sanity check
            }
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
  }


  $(document).ready( function() {
    $('#login_form').on('submit', function(event){
        event.preventDefault();
        console.log("form submitted!")  // sanity check
        login_failed();
    });
  });

//remove post
$('.remove-post-button').click(removePost);
  function removePost(event){
        event.preventDefault();
        var $this = $(this);
        var $post = $this.parents('.post-card');
        var post_id = $post.data('post-id');

        $.ajax({
            url : "/Blog/remove_post/", // the endpoint
            type : "POST", // http method
            data : {    post_id: post_id,
            }, // data sent with the post request

            // handle a successful response
            success : function(json) {
                console.log(json);

                $post.remove();
            },

            // handle a non-successful response
            error : function(xhr,errmsg,err) {
                $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                    " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    }

$('.duplicar').click(duplicar_post);
  function duplicar_post(event) {
        var $this = $(this);
        var $post = $this.parents('.post-card');
        var post_id = $post.data('post-id'); 
    $.ajax({
        url : "/Blog/duplicar/",
        type : "POST",
        data : { post_id: post_id},
        success : function(json) {
            
            //$('#text_area').val(''); // remove the value from the input
            //console.log(json); // log the returned json to the console            
            //$("#posts").append("<tr><td class='card'>"+json.id+"</td><td>"+json.author+"</td><td> "+json.text+"</td><td class='remove-post-button'><button>Borrar</button></td><td class='duplicar'><button>Duplicar</button></td>");            
            //console.log("success"); // another sanity check
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
  }


  // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});
