from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^post/(?P<post_id>[0-9]+)/$', views.post_show, name='post_detail'),
    url(r'^profile/(?P<user_required_name>[\s\S]+)/add_post/$', views.add_post, name='add_post'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^profile/(?P<user_required_name>[\s\S]+)/$', views.user_profile, name='user_profile'),
    url(r'remove_post/$', views.remove_post, name='remove_post'),
    url(r'duplicar/$', views.duplicar, name='duplicar'),
]
