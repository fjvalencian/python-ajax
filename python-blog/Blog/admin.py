from django.contrib import admin
from .models import Post, UserProfile, AppJSON

admin.site.register(Post)
admin.site.register(UserProfile)
admin.site.register(AppJSON)
