from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post, AppJSON
from .forms import PostForm, UserForm, UserProfileForm
from django.utils import timezone
from django.template.context_processors import csrf
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from copy import deepcopy
import json




def index(request):
    posts = Post.objects.all()
    form = PostForm()
    context = {'posts': posts, 'form': form}
    return render(request, 'Blog/index.html', context)

def post_show(request, post_id):
    post = Post.objects.filter(id = int(post_id))
    if len(post) > 0:
        return HttpResponse(ppost[0].post_text)
    else:
        return HttpResponse("No post with id ", post_id)

def add_post(request, user_required_name):
    if request.method == 'POST':
        form = PostForm(request.POST)

        myPost = Post(post_text = request.POST.get('post_text'),
                            post_likes = 0,
                            post_author = request.user,
                            pub_date = timezone.now())
        myPost.save()
        response_data = dict()
        response_data['text'] = request.POST.get('post_text')
        response_data['author'] = request.user.username
        response_data['id'] = myPost.id
        print(response_data)
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    else:
        form = PostForm()
    return HttpResponseRedirect('/Blog/')

def comentarios(request):
    c = models.Comentarios.objects.all()
    print(c)
    render_to_response('profile.html',{'c':c})


    
         


def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
            profile.save()
            registered = True
        else:
            print (user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
    return render(request,
            'Blog/register.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return  HttpResponse(
                        json.dumps({'redirect': "/Blog/"}),
                        content_type="application/json"
                                    )
            else:
                return HttpResponse("Your Blog account is disabled.")
        else:
            return HttpResponse(
                    json.dumps({}),
                    content_type="application/json"
                                )
    else:
        return render(request, 'Blog/login.html', {})

from django.contrib.auth import logout

# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/Blog/')

@login_required
def user_profile(request, user_required_name):
    #table = (AppJSON.objects.all()[0]).json_file.url
    #print(table)
    user_required = User.objects.filter(username = user_required_name)
    if len(user_required) >0:
        posts = Post.objects.filter(post_author = user_required[0])
        form = PostForm()
        user_required_id = user_required[0].id
        context = {'posts': posts, 'form': form, 'user_required_id': user_required_id}
        return render(request, 'Blog/profile.html', context)
    else:
        return HttpResponse("No user with name ", user_required_name)


@login_required
def remove_post(request):
    post_id = request.POST.get('post_id')
    post = Post.objects.get(id = post_id)

    post.delete()

    response_data = {}
    response_data['id'] = post_id
    response_data['result'] = 'post deleted'

    return HttpResponse(
        json.dumps(response_data),
        content_type="application/json"
    )

@login_required
def duplicar(request):
    post_id = request.POST.get('post_id')
    post = Post.objects.get(id = post_id)
    post = deepcopy(post)
    post.id = None 
    post.save()
    response_data = {}   
    return HttpResponse(
        json.dumps(response_data),
        content_type="application/json"
        )
