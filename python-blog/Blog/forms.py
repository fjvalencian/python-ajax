from django import forms
from django.contrib.auth.models import User
from Blog.models import UserProfile

class PostForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(
                        attrs={'id': 'text_area', 'required': True,'placeholder': 'Escribe algo...', 'rows':2}))

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('picture', 'role')
