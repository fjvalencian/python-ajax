from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    post_text = models.TextField()
    post_likes = models.BigIntegerField()
    post_author = models.ForeignKey(User)
    pub_date = models.DateTimeField('date published')

class Comentarios(models.Model):
    post = models.ForeignKey("Post")
    comentarios = models.TextField(max_length=100)        

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    picture = models.ImageField(upload_to='profile_images', blank=True)
    role = models.CharField(max_length=50)

    def __str__(self):
        return self.user.username

class AppJSON(models.Model):
	json_file = models.FileField(upload_to='jsonfiles')
