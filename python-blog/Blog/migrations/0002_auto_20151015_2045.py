# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Blog', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='comentarios',
        ),
        migrations.AddField(
            model_name='comentarios',
            name='post',
            field=models.ForeignKey(to='Blog.Post', default=None),
            preserve_default=False,
        ),
    ]
